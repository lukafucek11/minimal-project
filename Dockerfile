FROM node:12.19.0-slim

WORKDIR /usr/src/app
COPY package.json .
COPY . .

RUN yarn
RUN yarn run build

EXPOSE 3000
CMD [ "yarn", "start"]